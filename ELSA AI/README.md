# AI Service Performance Testing


## Getting started

#### Step 1: Install Java 8+

#### Step 2: Install Jmeter

#### Step 3: Download latest script

<li> GPT-⍺.jmx

<li> eventHandler.bsh

<li> TestData.csv

#### Step 4: Import script into Jmeter

#### Step 5: Adjust pathname of eventHandler.bsh & TestData.csv & Result.jtl

#### Step 6: Configure number of concurrent user, ramp-up period, loop count (will be discussed in Slack before running the script)

#### Step 7: Run the test 

#### Step 8: Wait for the test finished and send the Result.jtl file to the Slack channel

Pls refer to [this document](https://elsacorp.atlassian.net/wiki/spaces/POD/pages/2868314124/AI+Tutor+Service+Performance+Testing+Strategy) for more information